<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Config extends Model
{
    use HasFactory;

    // types
    public const TYPE_STRING = 1;
    public const TYPE_FLOAT = 2;
    public const TYPE_INT = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'key',
        'type',
        'value'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
    ];

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////// Helping Methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // keyword set
    public const CODE_PRODUCT_VAT = 'p_vat';

    /**
     * Getting Specific Config Details
     *
     * @param string $keyCode
     *
     * @return Config
     */
    public static function getConfigData(string $keyCode) : ?Config
    {
        $cofig = Config::select('*')
                ->where('key', '=', $keyCode)
                ->first();

        if(!$cofig) {
            throw new SystemException(
                'configuration_not_found',
                APIHelper::INVALID_DATA,
                [],
                'Configuration Not Found',
            );
        }

        return $cofig;
    }

     /**
     * Getting Specific Config Details Via Array
     * 
     * @param array $keyCodes
     * 
     * @return Config
     */
    public static function getConfigDataArray(array $keyCodes) : array
    {
        $outData = [];

        $configs = Config::select('*')
                ->whereIn('key', $keyCodes)
                ->get()
                ->toArray();

        foreach ($configs as $key => $config) {
            $outData[$config['key']] = [
                'id' => $config['id'],
                'value' => $config['value'],
            ];
        }

        return $outData;
    }
}
