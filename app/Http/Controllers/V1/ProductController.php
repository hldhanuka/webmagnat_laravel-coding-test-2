<?php

namespace App\Http\Controllers\V1;

use App\Models\Config;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\ProductAttribute;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateConfigRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Auth\Access\AuthorizationException;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('getAllProducts', Product::class);

            // initial data
            $outData = [];

            $products = Product::all();

            foreach ($products as $key => $product) {
                $productAttributes = $product->attributes;

                // get colours attributes
                $productAttributesColour = $productAttributes['colour'];

                // get size attributes
                $productAttributesSize = $productAttributes['size'];

                // set to colour details
                $productAttributesColourObject = ProductAttribute::find($productAttributesColour);

                // set to size details
                $productAttributesSizeObject = ProductAttribute::find($productAttributesSize);

                $outData[] = [
                    'id' => $product->id,
                    'product_name' => $product->product_name,
                    'price' => $product->price,
                    'price_with_vat' => $product->price_with_vat,
                    'photo' => env("APP_URL") . '/' . config("configdata.PRODUCT_PHOTO_BASE_FILE_UPLOAD_STORAGE_PATH") . $product->photo,
                    'attributes' => [
                        "colours" => [
                            'id' => @$productAttributesColourObject->id,
                            'type' => @$productAttributesColourObject->type,
                            'value' => @$productAttributesColourObject->value,
                        ],
                        "size" => [
                            'id' => @$productAttributesSizeObject->id,
                            'type' => @$productAttributesSizeObject->type,
                            'value' => @$productAttributesSizeObject->value,
                        ]
                    ]
                ];
            }

            return response(
                $outData
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $this->authorize('showProduct', Product::class);

            $product = Product::find($id);

            if(!$product) {
                return response(
                    [
                        'errors' => 'The requested Product is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $productAttributes = $product->attributes;

            // get colours attributes
            $productAttributesColour = $productAttributes['colour'];

            // get size attributes
            $productAttributesSize = $productAttributes['size'];

            // set to colour details
            $productAttributesColourObject = ProductAttribute::find($productAttributesColour);

            // set to size details
            $productAttributesSizeObject = ProductAttribute::find($productAttributesSize);

            return response(
                [
                    'id' => $product->id,
                    'product_name' => $product->product_name,
                    'price' => $product->price,
                    'price_with_vat' => $product->price_with_vat,
                    'photo' => env("APP_URL") . '/' . config("configdata.PRODUCT_PHOTO_BASE_FILE_UPLOAD_STORAGE_PATH") . $product->photo,
                    'attributes' => [
                        "colours" => [
                            'id' => @$productAttributesColourObject->id,
                            'type' => @$productAttributesColourObject->type,
                            'value' => @$productAttributesColourObject->value,
                        ],
                        "size" => [
                            'id' => @$productAttributesSizeObject->id,
                            'type' => @$productAttributesSizeObject->type,
                            'value' => @$productAttributesSizeObject->value,
                        ]
                    ]
                ]
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        try {
            $this->authorize('storeProduct', Product::class);

            // collect initial data
            $price = (float) $request->input('price');
            $priceWithVat = (float) $request->input('price');
            $photo = $request->photo;
            $attributes = $request->input('attributes');

            // file image details
            $imageName = date('YmdHis') . "." . $photo->extension();

            // get product vat config
            $config = Config::getConfigData(Config::CODE_PRODUCT_VAT);

            if($config) {
                $configVal = (float) $config->value;

                $priceWithVat = $price + $price * $configVal;
            }

            // ------------ product attributes validations
            // filtered attributes
            $attributes = json_decode($attributes);

            $colourAttriduteId = $attributes->colour;
            $sizeAttriduteId = $attributes->size;

            $colourAttributeObject = ProductAttribute::select('*')
                ->where('id', '=', $colourAttriduteId)
                ->where('type', '=', ProductAttribute::TYPE_COLOUR)
                ->first();

            if(!$colourAttributeObject) {
                return response(
                    [
                        'errors' => 'The requested Colour Attribute is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $sizeAttributeObject = ProductAttribute::select('*')
                ->where('id', '=', $sizeAttriduteId)
                ->where('type', '=', ProductAttribute::TYPE_SIZE)
                ->first();

            if(!$sizeAttributeObject) {
                return response(
                    [
                        'errors' => 'The requested Size Attribute is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
            // ------------ product attributes validations

            $product = Product::create(
                [
                    'product_name' => $request->input('product_name'),
                    'price' => $price,
                    'price_with_vat' => $priceWithVat,
                    'photo' => $imageName,
                    'attributes' => $attributes
                ]
            );

            // ------------ save the image to the path
            // get storage directory
            $storageDisk = Storage::disk('product_photo');

            // save file to the path
            $storageDisk->put($imageName, file_get_contents($photo));
            // ------------ save the image to the path

            return response($product);
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        try {
            $this->authorize('updateProduct', Product::class);

            $product = Product::find($id);

            if(!$product) {
                return response(
                    [
                        'errors' => 'The requested Product is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            // collect initial data
            $price = (float) $request->input('price');
            $priceWithVat = (float) $request->input('price');
            $photo = @$request->photo;
            $attributes = $request->input('attributes');

            // file image details
            $imageName = ($photo ? date('YmdHis') . "." . $photo->extension() : "");
            $imageOldName = $product->photo;

            // get product vat config
            $config = Config::getConfigData(Config::CODE_PRODUCT_VAT);

            if($config) {
                $configVal = (float) $config->value;

                $priceWithVat = $price + $price * $configVal;
            }

            // ------------ product attributes validations
            // filtered attributes
            $attributes = json_decode($attributes);

            $colourAttriduteId = $attributes->colour;
            $sizeAttriduteId = $attributes->size;

            $colourAttributeObject = ProductAttribute::select('*')
                ->where('id', '=', $colourAttriduteId)
                ->where('type', '=', ProductAttribute::TYPE_COLOUR)
                ->first();

            if(!$colourAttributeObject) {
                return response(
                    [
                        'errors' => 'The requested Colour Attribute is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $sizeAttributeObject = ProductAttribute::select('*')
                ->where('id', '=', $sizeAttriduteId)
                ->where('type', '=', ProductAttribute::TYPE_SIZE)
                ->first();

            if(!$sizeAttributeObject) {
                return response(
                    [
                        'errors' => 'The requested Size Attribute is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
            // ------------ product attributes validations

            $product->product_name = $request->input('product_name');
            $product->price = $price;
            $product->price_with_vat = $priceWithVat;

            if($photo) {
                $product->photo = $imageName;
            }

            $product->attributes = $attributes;

            $product->update();

            if($photo) {
                // get storage directory
                $storageDisk = Storage::disk('product_photo');

                // ------------ remove old image from the path
                // check old file
                if($storageDisk->exists($imageOldName)) {
                    $storageDisk->delete($imageOldName);
                }
                // ------------ remove old image from the path

                // ------------ save the image to the path
                // save file to the path
                $storageDisk->put($imageName, file_get_contents($photo));
                // ------------ save the image to the path
            }

            return response($product);
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->authorize('destroyProduct', Product::class);

            $product = Product::find($id);

            if(!$product) {
                return response(
                    [
                        'errors' => 'The requested Product is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $fileName = $product->photo;

            $product->delete();

            // get storage directory
            $storageDisk = Storage::disk('product_photo');

            // ------------ remove old image from the path
            // check old file
            if($storageDisk->exists($fileName)) {
                $storageDisk->delete($fileName);
            }
            // ------------ remove old image from the path

            return response(
                [
                    'message' => 'This user deleted successfully'
                ],
                Response::HTTP_OK
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }
}