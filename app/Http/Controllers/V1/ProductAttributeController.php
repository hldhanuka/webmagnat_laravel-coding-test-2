<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\ProductAttribute;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateConfigRequest;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\StoreProductAttributeRequest;
use App\Http\Requests\UpdateProductAttributeRequest;

class ProductAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('getAllProductAttributes', ProductAttribute::class);

            $productAttribute = ProductAttribute::all();

            return response(
                $productAttribute
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $this->authorize('showProductAttribute', ProductAttribute::class);

            $productAttribute = ProductAttribute::find($id);

            if(!$productAttribute) {
                return response(
                    [
                        'errors' => 'The requested Product Attribute is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            return response(
                $productAttribute
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductAttributeRequest $request)
    {
        try {
            $this->authorize('storeProductAttribute', ProductAttribute::class);

            $productAttribute = ProductAttribute::create(
                [
                    'type' => $request->input('type'),
                    'value' => $request->input('value')
                ]
            );

            return response($productAttribute);
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductAttributeRequest $request, $id)
    {
        try {
            $this->authorize('updateProductAttribute', ProductAttribute::class);

            $productAttribute = ProductAttribute::find($id);

            if(!$productAttribute) {
                return response(
                    [
                        'errors' => 'The requested Product Attribute is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $productAttribute->type = $request->input('type');
            $productAttribute->value = $request->input('value');

            $productAttribute->update();

            return response($productAttribute);
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->authorize('destroyProductAttribute', ProductAttribute::class);

            $productAttribute = ProductAttribute::find($id);

            if(!$productAttribute) {
                return response(
                    [
                        'errors' => 'The requested Product Attribute is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $productAttribute->delete();

            return response(
                [
                    'message' => 'This product attribute deleted successfully'
                ],
                Response::HTTP_OK
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }
}