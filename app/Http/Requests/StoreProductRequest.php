<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => [
                'required',
                'unique:products,product_name',
                'max:255'
            ],
            'price' => [
                'regex:/' . config("configdata.REGEX.amount") . '/'
            ],
            'photo' => [
                'required',
                'mimes:' . implode(",", config("configdata.IMAGE_EXTENSIONS")),
                'max:' . config("configdata.IMAGE_SIZE.max"),
                'min:' . config("configdata.IMAGE_SIZE.min"),
            ],
            'attributes' => [
                'required',
                'json'
            ]
        ];
    }
}
