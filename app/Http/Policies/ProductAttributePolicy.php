<?php

namespace App\Http\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductAttributePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can get all product attributes.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function getAllProductAttributes(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can show product attribute.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function showProductAttribute(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can store product attribute.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function storeProductAttribute(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can update product attribute.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function updateProductAttribute(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can delete product attribute.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function destroyProductAttribute(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }
}