<?php

namespace App\Http\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can get all products.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function getAllProducts(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can show product.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function showProduct(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can store product.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function storeProduct(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can update product.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function updateProduct(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can delete product.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function destroyProduct(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }
}