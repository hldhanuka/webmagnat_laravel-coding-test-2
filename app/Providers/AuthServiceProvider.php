<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Config;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Http\Policies\UserPolicy;
use App\Http\Policies\ConfigPolicy;
use App\Http\Policies\ProductPolicy;
use App\Http\Policies\ProductAttributePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Config::class => ConfigPolicy::class,
        ProductAttribute::class => ProductAttributePolicy::class,
        Product::class => ProductPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
