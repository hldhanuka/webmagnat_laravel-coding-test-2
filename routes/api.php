<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\TaskController;
use App\Http\Controllers\V1\UserController;
use App\Http\Controllers\V1\ConfigController;
use App\Http\Controllers\V1\ProductController;
use App\Http\Controllers\V1\ProjectController;
use App\Http\Controllers\V1\AuthenticationController;
use App\Http\Controllers\V1\ProductAttributeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::name('register')->post('/v1/register',[AuthenticationController::class, 'register']);

Route::name('login')->post('/v1/login', [AuthenticationController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::name('users')->get('/v1/users', [UserController::class, 'index']);
    Route::name('users')->get('/v1/users/{id}', [UserController::class, 'show']);
    Route::name('users')->post('/v1/users', [UserController::class, 'store']);
    Route::name('users')->put('/v1/users/{id}', [UserController::class, 'update']);
    Route::name('users')->delete('/v1/users/{id}', [UserController::class, 'destroy']);
    Route::name('users')->patch('/v1/users/{id}', [UserController::class, 'edit']);

    Route::name('configs')->get('/v1/configs', [ConfigController::class, 'index']);
    Route::name('configs')->get('/v1/configs/{id}', [ConfigController::class, 'show']);
    Route::name('configs')->put('/v1/configs/{id}', [ConfigController::class, 'update']);

    Route::name('product-attributes')->get('/v1/product-attributes', [ProductAttributeController::class, 'index']);
    Route::name('product-attributes')->get('/v1/product-attributes/{id}', [ProductAttributeController::class, 'show']);
    Route::name('product-attributes')->post('/v1/product-attributes', [ProductAttributeController::class, 'store']);
    Route::name('product-attributes')->put('/v1/product-attributes/{id}', [ProductAttributeController::class, 'update']);
    Route::name('product-attributes')->delete('/v1/product-attributes/{id}', [ProductAttributeController::class, 'destroy']);

    Route::name('products')->get('/v1/products', [ProductController::class, 'index']);
    Route::name('products')->get('/v1/products/{id}', [ProductController::class, 'show']);
    Route::name('products')->post('/v1/products', [ProductController::class, 'store']);
    Route::name('products')->post('/v1/products/{id}',[ProductController::class, 'update']);
    Route::name('products')->delete('/v1/products/{id}', [ProductController::class, 'destroy']);

    Route::name('logout')->post('/v1/logout', [AuthenticationController::class, 'logout']);
});