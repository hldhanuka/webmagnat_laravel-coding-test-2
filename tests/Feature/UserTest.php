<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Role;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example to create the user
     *
     * @return void
     */
    public function test_user_create()
    {
        // clear the all data
        $this->testInitiateAndClear();

        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $userData = [
            "username" => "test.admin",
            "password"=> "123456",
            "role_id" => Role::ROLE_TYPE_ADMIN,
            "password_confirmed"=> "123456",
        ];

        $outData = $this->post(
            '/api/v1/users',
            $userData,
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }

    /**
     * A basic feature test example to update the user
     *
     * @return void
     */
    public function test_user_update()
    {
        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $userData = [
            "username" => "test.admin.2",
            "password"=> "123456",
            "role_id" => Role::ROLE_TYPE_ADMIN,
            "password_confirmed"=> "123456",
        ];

        $outData = $this->put(
            '/api/v1/users/3',
            $userData,
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }

    /**
     * A basic feature test example to delete the user
     *
     * @return void
     */
    public function test_user_delete()
    {
        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $outData = $this->delete(
            '/api/v1/users/3',
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }
}
