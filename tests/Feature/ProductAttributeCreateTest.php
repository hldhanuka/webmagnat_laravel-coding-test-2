<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Role;
use App\Models\ProductAttribute;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductAttributeCreateTest extends TestCase
{
    /**
     * A basic feature test example to create the product attribute
     *
     * @return void
     */
    public function test_product_attribute_create()
    {
        // clear the all data
        $this->testInitiateAndClear();

        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $productAttributeData = [
            "type" => ProductAttribute::TYPE_COLOUR,
            "value"=> "White",
        ];

        $outData = $this->post(
            '/api/v1/product-attributes',
            $productAttributeData,
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }

     /**
     * A basic feature test example to update the product attributes
     *
     * @return void
     */
    public function test_product_attribute_update()
    {
        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $productAttributeData = [
            "type" => ProductAttribute::TYPE_COLOUR,
            "value"=> "Orage",
        ];

        $outData = $this->put(
            '/api/v1/product-attributes/3',
            $productAttributeData,
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }

    /**
     * A basic feature test example to delete the product attribute
     *
     * @return void
     */
    public function test_product_attribute_delete()
    {
        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $outData = $this->delete(
            '/api/v1/product-attributes/3',
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }
}