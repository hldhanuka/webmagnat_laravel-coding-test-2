<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {              
        Config::insert(
            [
                'key' => 'p_vat',
                'type' => Config::TYPE_FLOAT,
                'value' => (float) 0.10,
            ]
        );
    }
}