<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductAttribute;

class ProductAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductAttribute::insert(
            [
                'type' => ProductAttribute::TYPE_COLOUR,
                'value' => "Black"
            ]
        );

        ProductAttribute::insert(
            [
                'type' => ProductAttribute::TYPE_SIZE,
                'value' => 10
            ]
        );
    }
}
