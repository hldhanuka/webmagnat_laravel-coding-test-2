<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\ConfigSeeder;
use Database\Seeders\ProductSeeder;
use Database\Seeders\ProductAttributeSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(
            [
                RoleSeeder::class,
                UserSeeder::class,
                ConfigSeeder::class,
                ProductAttributeSeeder::class,
                ProductSeeder::class
            ]
        );
    }
}
