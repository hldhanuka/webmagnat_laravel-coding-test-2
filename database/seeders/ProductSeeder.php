<?php

namespace Database\Seeders;

use App\Models\Config;
use App\Models\Product;
use Illuminate\Database\Seeder;
use App\Models\ProductAttribute;
use Illuminate\Support\Facades\Storage;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fileName = "20230407110900.jpg";
        $price = (float) 10.00;
        $priceWithVat = $price;

        // get product vat config
        $config = Config::getConfigData(Config::CODE_PRODUCT_VAT);

        if($config) {
            $configVal = (float) $config->value;

            $priceWithVat = $price + $price * $configVal;
        }

        // get storage directory
        $storageDisk = Storage::disk('product_photo');

        // check old file
        if($storageDisk->exists($fileName)) {
            Product::insert(
                [
                    'product_name' => 'Apple iPhone',
                    'price' => $price,
                    'price_with_vat' => $priceWithVat,
                    'photo' => $fileName,
                    'attributes' => json_encode(
                        [
                            "colour" => 1,
                            "size" => 2
                        ]
                    )
                ]
            );
        }
    }
}
